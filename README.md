z80dasm 
=======

This is a **unnoficial repository** maintained by Lisias T. See [here](https://www.tablix.org/~avian/z80dasm/) for the canonical repository.

For more information, please read:

* [README](README)
* [NEWS](NEWS)
* [ChangeLog](ChangeLog)
* [AUTHORS](AUTHORS)

Disclaimer
----------
Lisias T **is not** the maintainer of the software, just of this repository.

This repository is for source code reference only. **Do not** rely on it for official information or code.

